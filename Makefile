all: alltests coverage launch

alltests:
	coverage run -m unittest project.tests.test_category project.tests.test_listing

coverage:
	coverage report

launch:
	PYTHONPATH=${PYTHONPATH}:"./"
	python3 project/__init__.py
