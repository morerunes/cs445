import json

from datetime import datetime, timedelta

import time

from project.data.category import CategoryEncoder


class Listing:
    def __init__(self, title='DEFAULT'):
        self.title = title
        self.price = 0.0
        self.description = 'DEFAULT'
        self.image = None
        self.link = '#'
        self.categories = []
        self.startDate = datetime.now()
        self.endDate = self.startDate + timedelta(days=1)
        self.featured = False

    def add_category(self, category):
        self.categories.append(category)

    def feature(self):
        self.featured = True

    def __eq__(self, other):
        return self.title == other.title and self.description == other.description


class ListingEncoder(json.JSONEncoder):
    def default(self, o):
        if type(o) is not datetime:
            return o.__dict__
        else:
            return [str(o)[:-7]]

    @staticmethod
    def from_json(dct):
        # return self until we hit the root
        if 'title' not in dct:
            return dct
        # now that we are at the root, process self and categories

        listing = Listing(dct['title'])
        listing.categories = [CategoryEncoder.from_json(dct2) for dct2 in dct['categories']]
        listing.description = dct['description']
        listing.price = dct['price']
        listing.image = dct['image']
        listing.link = dct['link']
        listing.startDate = time.strptime(dct['startDate'][0], '%Y-%m-%d %H:%M:%S')
        listing.endDate = datetime.strptime(dct['endDate'][0], '%Y-%m-%d %H:%M:%S')
        return listing
