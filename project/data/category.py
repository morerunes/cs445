import json

from datetime import datetime


class Category:
    def __init__(self, name='DEFAULT', parent=None):

        self.parent = parent
        if parent is not None:
            assert isinstance(parent, Category)
            parent.add_child_category(self)
        self.category_name = name
        self.children = []

    def add_child_category(self, child):
        self.children.append(child)

    def __str__(self):
        if self.parent is not None:
            return self.category_name
        else:
            return self.category_name + ' (child of ' + str(self.parent) + ')'

    def __eq__(self, other):
        return self.category_name == other.category_name


class CategoryEncoder(json.JSONEncoder):
    def default(self, o):
        if type(o) is not datetime:
            return o.__dict__
        else:
            return [str(o)[:-7]]

    def from_json(self):
        category = Category(self['category_name'])
        category.parent = self['parent']
        category.children = self['children']
        return category
