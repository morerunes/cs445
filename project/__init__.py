# server functionality
import json
import uuid
import sys
sys.path.insert(1,'./')
from flask import Flask, request

from project.data.category import Category, CategoryEncoder
from project.data.listing import Listing, ListingEncoder

app = Flask(__name__)
app.debug = True
listings = {}
categories = {}


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/listings', methods=['GET', 'POST'])
@app.route('/listings/<listing_id>')
def get_listings(listing_id=None):
    if request.method == 'GET':
        if listing_id is not None:
            return json.dumps(listings[listing_id], cls=ListingEncoder)
        else:
            return json.dumps(listings, cls=ListingEncoder)
    elif request.method == 'POST':
        # adds the listing, returns its identifier
        listing_id = str(uuid.uuid4())
        listing = json.loads(request.data.decode('utf-8'))
        listings[listing_id] = listing
        return str(listing_id)


@app.route('/listings/bycategory/<category_name>')
def listings_by_category(category_name):
    category = categories[category_name]

    if category is None:
        return json.dumps([])
    else:
        res = []
        for listing in listings.values():
            if category in listing.categories:
                res.append(listing)

        return json.dumps(res, cls=ListingEncoder)


@app.route('/category', methods=['GET', 'POST'])
@app.route('/category/<category_name>')
def category_functions(category_name=None):
    if request.method == 'GET':
        if category_name is not None:
            return json.dumps(categories[category_name].__dict__)
        else:
            return json.dumps(categories.__dict__)
    elif request.method == 'POST':
        data = json.loads(request.data.decode('utf-8'))
        category = Category(data['category_name'])
        category.children = data['children']
        category.parent = data['parent']

        if category not in categories.values():
            add_category(category)

        return 'OK'


def add_category(category):
    # check parent first, create if necessary
    if category.parent is not None and category.parent.category_name not in categories.keys():
        add_category(category.parent)

    if category not in categories.values():
        categories[category.category_name] = category


if __name__ == '__main__':
    app.run()
