Welcome to Aaron Echols' CS445 Final Project

When operating, this code will allow you to make requests against
the local host to create and manage advertisement listings. Each listing
has a description, a title, a price, a link, an image, and a set of
categories.

A listing can be featured by category, or by being 'front page featured'
which will make it show up earlier in unbounded searches.

Please feel free to make requests with the sample JSON files in the samples folder.

// -- BUILDING AND RUNNING -- //

you will need to have python3 installed, as well as python3-pip, flask, coverage

included makefile can be used to run tests, view coverage, or launch server

to run tests, coverage, and start the server run make

coverage goes over a bunch of stuff, but the important bits are in my project module

// -- API DESCRIPTION -- //

all url paths (/listings, /category, etc) shall be prefixed. console will output path.

(something like http://127.0.0.1:5000/)

- To create a listing, POST json to /listings (see example create_listing.json),
it will return the listing-id in plain text

- To retrieve a listing, GET /listings/<listing-id>

- To view all listings (no pagination, non-category featured listings first), GET /listings

- To retrieve listings by category, GET /listings/bycategory/<category name>

- To create a category, POST json to /category (see example create_category.json)
