import unittest, json
import project
from project.data.listing import Listing


class TestListings(unittest.TestCase):
    def setUp(self):
        project.app.config['TESTING'] = True
        self.app = project.app.test_client()

    def tearDown(self):
        project.listings = {}

    def testDefaultTitle(self):
        self.assertEqual('DEFAULT', Listing().title)

    def testNonDefaultTitle(self):
        self.assertEqual('nondefault', Listing('nondefault').title)

    def testDefaultListingParameters(self):
        listing = Listing()
        self.assertEqual(0.0, listing.price, 'default price')
        self.assertEqual('DEFAULT', listing.title, 'default title')
        self.assertEqual('DEFAULT', listing.description, 'default description')
        self.assertEqual(None, listing.image, 'default image')
        self.assertEqual('#', listing.link, 'default link')

    def testNonDefaultListingParameters(self):
        listing = Listing('nondefault')
        self.assertEqual('nondefault', listing.title)

        listing.title = 'nondefault2'
        listing.price = 1.2
        listing.description = 'test description'
        listing.image = {}
        listing.link = 'test link'

        self.assertEqual('nondefault2', listing.title)
        self.assertEqual(1.2, listing.price)
        self.assertEqual('test description', listing.description)
        self.assertEqual({}, listing.image, 'listing image is empty object')
        self.assertEqual('test link', listing.link)

    def testGetListings(self):
        rv = self.app.get('/listings')
        self.assertEqual('{}', rv.data.decode('utf-8'))

    def testAddListing(self):
        rv = self.app.post(path='/listings',
                           data=json.dumps({
                               'title': 'post test title',
                               'price': 1.2,
                               'description': 'test description',
                               'image': {},
                               'link': 'test link'
                           }),
                           content_type='application/json')

        self.assertEqual(200, rv.status_code, 'Should be able to POST new listing')
        listing_id = rv.data.decode('utf-8')

        rv = json.loads(self.app.get('/listings').data.decode('utf-8'))

        self.assertEqual(rv[listing_id]['title'], 'post test title')

    def testRetrieveListingByID(self):
        rv = self.app.post(path='/listings',
                           data=json.dumps({
                               'title': 'retrieval test',
                               'price': 1.2,
                               'description': 'test description',
                               'image': {},
                               'link': 'test link'
                           }),
                           content_type='application/json')

        self.assertEqual(200, rv.status_code)
        listing_id = rv.data.decode('utf-8')

        rv = self.app.get('/listings/' + listing_id)
        rlisting = json.loads(rv.data.decode('utf-8'))
        self.assertEqual('retrieval test', rlisting['title'])


if __name__ == '__main__':
    unittest.main()
