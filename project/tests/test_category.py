import unittest, json
import project
from project.data.category import Category, CategoryEncoder
from project.data.listing import Listing, ListingEncoder


class TestCategory(unittest.TestCase):
    def setUp(self):
        project.app.config['TESTING'] = True
        self.app = project.app.test_client()

    def tearDown(self):
        project.categories = {}

    def test_basic_category(self):
        self.assertEqual(Category().category_name, 'DEFAULT')
        self.assertEqual(Category('nondefault').category_name, 'nondefault')

    def test_sub_category(self):
        cat1 = Category('vehicles')
        cat2 = Category('cars', cat1)

        self.assertIn(cat2, cat1.children)

    def test_add_category_to_app(self):
        category = Category('test category')
        self.app.post('/category', data=json.dumps(category, cls=CategoryEncoder))
        res = json.loads(self.app.get('/category/test category').data.decode('utf-8'),
                         object_hook=CategoryEncoder.from_json)
        self.assertEqual(category, res)

    def test_get_listing_by_category(self):
        category = Category('test category')
        listing = Listing('test by category')
        listing.add_category(category)

        if len(project.categories) == 0:
            self.app.post('/category', data=json.dumps(category, cls=CategoryEncoder))

        project.listings['test'] = listing
        res = json.loads(self.app.get('/listings/bycategory/' + 'test category').data.decode('utf-8'),
                         object_hook=ListingEncoder.from_json)

        self.assertIn(listing, res)

